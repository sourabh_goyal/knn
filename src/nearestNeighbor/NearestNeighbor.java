package nearestNeighbor;

public class NearestNeighbor implements Comparable<NearestNeighbor>{
	double distance;
	String Label;
	public NearestNeighbor(double distance, String label) {
		this.distance = distance;
		this.Label = label;
	}
	 
	public double compare(NearestNeighbor d, NearestNeighbor d1){
      return d.distance - d1.distance;
   }

	@Override
	public int compareTo(NearestNeighbor o) {
		
		double difference= distance - o.distance;
		if (difference > 0)
		{
			return 1;
		}
		else if (difference<0)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
}
