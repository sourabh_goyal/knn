package nearestNeighbor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class NearestNeighborMain {

	public static void main(String[] args) {
		try {
			File Trainingdataset = new File(args[0]);
			File Testingdataset = new File(args[1]);

			BufferedReader Reader = new BufferedReader(new FileReader(
					Trainingdataset));
			BufferedReader inputS = new BufferedReader(new InputStreamReader(
					System.in));

			System.out.println("Enter the value of K");
			int KofAlgo = Integer.parseInt(inputS.readLine());

			System.out
					.println("Enter the number of features you want to use for calculation: ");
			int num_features = Integer.parseInt(inputS.readLine());

			int feature_position[] = new int[num_features];

			for (int i = 0; i < num_features; i++) {
				System.out.println("Enter the feature " + (i + 1)
						+ "th position");
				feature_position[i] = Integer.parseInt(inputS.readLine()) - 1;
			}

			System.out.println("Enter the class position in dataset: ");
			int classPosition = Integer.parseInt(inputS.readLine()) - 1;

			Set<String> elementClass = new HashSet<String>();
			ArrayList<String[]> TrainingData = new ArrayList<String[]>();
			String line = "";

			while ((line = Reader.readLine()) != null) {
				String[] fields = line.split(",");
				TrainingData.add(fields);
				elementClass.add(fields[classPosition]);
			}

			int numClasses = elementClass.size();

			Map<Integer, String> classTypes = new HashMap<Integer, String>();

			Iterator<String> itr = elementClass.iterator();

			int key = 0;

			while (itr.hasNext()) {
				classTypes.put(key, (String) itr.next());
				key++;
			}
			System.out.println(classTypes);
			Reader.close();

			int confusion_matrix[][] = new int[numClasses][numClasses];
			ArrayList<NearestNeighbor> outputDataset = new ArrayList<NearestNeighbor>();
			double querycords[] = new double[num_features];
			double Trainingcords[] = new double[num_features];
			double EuclideanDistance = 0;
			double SqOfCordDistance[] = new double[num_features];
			double SumOfSquares = 0;
			int countMatrix[] = new int[numClasses];

			String Predictedclass = "";
			String ActualClass = "";
			int indexOfMaxVal = 0;
			int keyOfActualClass = 0;
			int keyOfPredictedClass = 0;

			Reader = new BufferedReader(new FileReader(Testingdataset));

			while ((line = Reader.readLine()) != null) {
				Arrays.fill(countMatrix, 0);
				String[] fields = line.split(",");
				SumOfSquares = 0;
				outputDataset.clear();
				Predictedclass = "";
				ActualClass = "";
				indexOfMaxVal = 0;

				for (int i = 0; i < num_features; i++) {
					querycords[i] = Double
							.parseDouble(fields[feature_position[i]]);
				}

				for (int i = 0; i < TrainingData.size(); i++) {

					String TrainingArray[] = TrainingData.get(i);
					SumOfSquares = 0;
					for (int j = 0; j < num_features; j++) {
						Trainingcords[j] = Double
								.parseDouble(TrainingArray[feature_position[j]]);
						SqOfCordDistance[j] = Math.pow(
								(Trainingcords[j] - querycords[j]), 2);
						SumOfSquares = SumOfSquares + SqOfCordDistance[j];
					}
					EuclideanDistance = Math.sqrt(SumOfSquares);
					outputDataset.add(new NearestNeighbor(EuclideanDistance,
							TrainingArray[classPosition]));
				}

				Collections.sort(outputDataset);

				String DataClass = "";
				for (int i = 0; i < KofAlgo; i++) {
					DataClass = outputDataset.get(i).Label;
					for (int j = 0; j < classTypes.size(); j++) {
						if (DataClass.equalsIgnoreCase(classTypes.get(j))) {
							countMatrix[j]++;
						}
					}
				}

				for (int i = 0; i < numClasses - 1; i++) {
					if (countMatrix[i] < countMatrix[i + 1]) {
						indexOfMaxVal = i + 1;
					}
				}
				keyOfPredictedClass = indexOfMaxVal;
				Predictedclass = (String) classTypes.get(keyOfPredictedClass);
				ActualClass = fields[classPosition];

				System.out.println("predicted class is " + Predictedclass);
				System.out.println("Actual Class is " + ActualClass);

				if (classTypes.containsValue(ActualClass)) {
					for (int j = 0; j < classTypes.size(); j++) {
						if (ActualClass.equalsIgnoreCase(classTypes.get(j))) {
							keyOfActualClass = j;
							break;
						}
					}
				} else {
					System.out.println("undetermined class in testing set");
				}

				confusion_matrix[keyOfActualClass][keyOfPredictedClass]++;

			}
			Reader.close();
			
			double sum_good =0;
			double sum_bad=0;
			double efficiency=0;
			

			for (int i = 0; i < numClasses; i++) {
				for (int j = 0; j < numClasses; j++) {
					System.out.print(confusion_matrix[i][j]);
					System.out.print("\t");
					if ((i==j))
					{
						sum_good=sum_good+confusion_matrix[i][j];
					}
					else
					{
						sum_bad=sum_bad+confusion_matrix[i][j];
					}
				}
				System.out.println("");
			}
			
			efficiency=100*sum_good/(sum_good+sum_bad);
			System.out.println("efficiency of the algorithm is: "+efficiency+"%");

		} catch (Exception noe) { System.out.println("wtf");

		}
	}

}
